How To Run the Project?
PRE-CONDITION:
- Installed JAVA
- Installed Maven
- Installed allure report
Config Step:
// Install dependency and plugin
- Run: mvn Install
// Run Test
- Run mvn clean test
//View Report:
- Run command line: allure server allure-results
-------------------
Headless mode: Default is true.
If you want to set it's false, go to Variables file and update the HEADLESS value.