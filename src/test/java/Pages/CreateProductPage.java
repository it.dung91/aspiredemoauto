package Pages;

import Variable.Variables;
import io.qameta.allure.Step;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;



public class CreateProductPage extends GeneralPage{
    WebDriver driver;
    Variables var = new Variables();
    @FindBy(xpath = "//input[@name='name']")
    WebElement PRODUCT_NAME_TXT;
    @FindBy(xpath = "//label[normalize-space()='Product Category']/../following-sibling::td//input" )
    WebElement PRODUCT_CATEGORY_DROPDOWN;
    @FindBy(xpath = "//li[@class='ui-menu-item']/a[normalize-space()='All / Saleable']")
    WebElement CATEGORY_SALEABLE;
    @FindBy(xpath = "//label[normalize-space()='Unit of Measure']/../following-sibling::td//input" )
    WebElement UNIT_OF_MEASURE_DROPDOWN;
    @FindBy(xpath = "//label[normalize-space()='Purchase Unit of Measure']/../following-sibling::td//input" )
    WebElement PURCHASE_UNIT_OF_MEASURE_DROPDOWN;
    @FindBy(xpath = "//select[@name='type']")
    WebElement PRODUCT_TYPE_DROPDOWN;
    @FindBy(xpath = "//input[@name='default_code']")
    WebElement INTERNAL_REFERENCE_TXT;
    @FindBy(xpath = "//input[@name='barcode']")
    WebElement BARCODE_TXT;
    @FindBy(xpath = "//DIV[@name='standard_price']/input")
    WebElement COST_TXT;
    @FindBy(xpath = "//textarea[@placeholder='This note is only for internal purposes.']")
    WebElement INTERNAL_NOTE_TXT;
    //Inventory tab
    @FindBy(xpath = "//li[@class='nav-item']/a[normalize-space()='Inventory']")
    WebElement INVENTORY_TAB;
    @FindBy(xpath = "//label[normalize-space()='Manufacture']")
    WebElement MANUFACTURE_CHECKBOX;
    @FindBy(xpath = "//input[@name='weight']")
    WebElement WEIGHT_TEXT;
    @FindBy(xpath = "//input[@name='volume']")
    WebElement VOLUME_TXT;
    @FindBy(xpath = "//input[@name='produce_delay']")
    WebElement MN_LEAD_TIME_TXT;
    @FindBy(xpath = "//input[@name='sale_delay']")
    WebElement CS_LEAD_TIME_TXT;
    @FindBy(xpath = "(//a[@role='button'][normalize-space()='Add a line'])[2]")
    WebElement INVENTORY_ADD_PACKAGING_BTN;
    @FindBy(xpath = "//textarea[@name='description_pickingout']")
    WebElement DELIVERY_DES_TXT;
    @FindBy(xpath = "//textarea[@name='description_pickingin']")
    WebElement RECEIPT_DES_TXT;
    @FindBy(xpath = "//textarea[@name='description_picking']")
    WebElement TRANSFER_DES_TXT;

    @FindBy(xpath = "//button[normalize-space()='Save']")
    WebElement SAVE_BTN;
    //Packaging popup
    @FindBy(xpath = "//h4[@class='modal-title']")
    WebElement PKG_POPUP_TITLE;
    @FindBy(xpath = "//label[normalize-space()='Packaging']/following-sibling::h1/input")
    WebElement PKG_NAME_TXT;
    @FindBy(xpath = "//input[@name='qty']")
    WebElement PKG_QTY_TXT;
    @FindBy(xpath = "(//input[@name='barcode'])[2]")
    WebElement PKG_BARCODE_TXT;
    @FindBy(xpath = "//span[normalize-space()='Save & Close']/..")
    WebElement PKG_SAVE_CLOSE_BTN;
    //Update quantity
    @FindBy(xpath = "//span[normalize-space()='Update Quantity']/..")
    WebElement UPDATE_QTY_BUTTON;
    @FindBy(xpath = "(//input[@class='o_input ui-autocomplete-input'])[1]")
    WebElement QTY_LOCATION;
    @FindBy(xpath = "(//input[@class='o_input ui-autocomplete-input'])[2]")
    WebElement QTY_PACKAGE;
    @FindBy(xpath = "//input[@name='inventory_quantity']")
    WebElement INVENTORY_QTY;
    @FindBy(xpath = "//p[@class='o_view_nocontent_empty_folder']")
    WebElement EMPTY_MESSAGE;
    @FindBy(xpath = "//div[@name='location_id']//a")
    WebElement LOCATION_DROPDOWN;
    @FindBy(xpath = "(//li[@class='ui-menu-item'])[1]")
    WebElement LOCCATION_ITEM;
    @FindBy(xpath = "//div[@class='o_Message_prettyBody']")
    WebElement CREATED_LOG;


    public CreateProductPage(WebDriver driver){
        super(driver);
    }

    @Step("Create Product")
    public void fillProductInformation(String productName, String productType, String internalRef, String internalNote,String barCode,String cost) {
        waitElementVisibility(PRODUCT_NAME_TXT, 20);
        PRODUCT_NAME_TXT.sendKeys(productName);
        //Select Product Type
        Select productTypeDr = new Select(PRODUCT_TYPE_DROPDOWN);
        productTypeDr.selectByVisibleText(productType);
        //Select Product Category
        clickElement(PRODUCT_CATEGORY_DROPDOWN);
        clickElement(CATEGORY_SALEABLE);
        //Fill internal reference
        fillFieldElement(INTERNAL_REFERENCE_TXT, internalRef);
        fillFieldElement(BARCODE_TXT, barCode);
        //Input COST
        fillFieldElement(COST_TXT, cost);
        fillFieldElement(INTERNAL_NOTE_TXT, internalNote);
    }
    @Step("Fill Inventory")
    public void fillInventoryData(String weight, String volume, String manufactureTime, String customerTime ){
        //Open INVENTORY Tab
        clickElement(INVENTORY_TAB);
        clickElement(MANUFACTURE_CHECKBOX);
        fillFieldElement(WEIGHT_TEXT,weight);
        fillFieldElement(VOLUME_TXT,volume);
        fillFieldElement(MN_LEAD_TIME_TXT,manufactureTime);
        fillFieldElement(CS_LEAD_TIME_TXT,customerTime);
        //fill Description
        fillFieldElement(DELIVERY_DES_TXT,"Automation test");
        fillFieldElement(RECEIPT_DES_TXT, "Automation test");
        fillFieldElement(TRANSFER_DES_TXT,"Automation test");
    }
    @Step("Add Packaging")
    public void addPackaging(String packageName, String quantity, String barcode){
        //Add Packaging
        clickElement(INVENTORY_ADD_PACKAGING_BTN);
        seeText(PKG_POPUP_TITLE,"Create Product Packages",10);
        fillFieldElement(PKG_NAME_TXT,packageName);
        fillFieldElement(PKG_QTY_TXT,quantity);
        fillFieldElement(PKG_BARCODE_TXT,barcode);
        clickElement(PKG_SAVE_CLOSE_BTN);
    }
    @Step("Update product quantity")
    public void updateQuantity(String qtyLocation, String qtyPackage,String quantity){
        clickElement(UPDATE_QTY_BUTTON);
        seeText(EMPTY_MESSAGE, "No Stock On Hand",10);
        clickButtonCreate();
        clickElement(QTY_LOCATION);
        clickElement(LOCCATION_ITEM);
        QTY_LOCATION.sendKeys(Keys.ENTER);
        fillFieldElement(QTY_PACKAGE,qtyPackage);
        QTY_PACKAGE.sendKeys(Keys.TAB);
        fillFieldElement(INVENTORY_QTY,quantity);
        clickSaveButton();
    }

    @Step
    public void verifyCreatedProduct(){
        seeText(CREATED_LOG, "Product Template created",10);
    }
}

