package Pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Calendar;
import java.util.Date;

public class GeneralPage {

    WebDriver driver;
    WebDriverWait wait;
    @FindBy(xpath = "//div[normalize-space()='Inventory']")
    WebElement INVENTORY_LABEL;
    @FindBy(xpath = "//div[normalize-space()='Manufacturing']")
    WebElement MANUFACTURE_LABEL;
    @FindBy(xpath = "//a[@class='o_menu_brand'][normalize-space()='Inventory']")
    WebElement INVENTORY_PAGE;
    @FindBy(xpath = "//a[@class='o_menu_brand'][normalize-space()='Manufacturing']")
    WebElement MANUFACTURE_PAGE;
    @FindBy(xpath = "//a[@title='Applications']")
    WebElement APPLICATION_ICON;
    @FindBy(xpath = "//a[@data-toggle='dropdown'][normalize-space()='Products']")
    WebElement PRODUCT_DROPDOWN;
    @FindBy(xpath = "//span[normalize-space()='Products']/ancestor::a")
    WebElement PRODUCT_MENU;
    @FindBy(xpath = "//button[normalize-space()='Create']")
    WebElement CREATE_BTN;
    @FindBy(xpath = "//li[contains(@class,'breadcrumb-item')][normalize-space()='Products']")
    WebElement PAGE_TITLE_NAVIGTION;
    @FindBy(xpath = "//button[normalize-space()='Save']")
    WebElement SAVE_BTN;


    public GeneralPage(WebDriver driver){
        this.driver=driver;
        PageFactory.initElements(driver,this);
    }
    public void waitElementVisibility(WebElement element, int timeout){
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(timeout));
        wait.until(ExpectedConditions.visibilityOf(element));
    }
    public void waitElementClickable(WebElement element, int timeout){
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(timeout));
        wait.until(ExpectedConditions.elementToBeClickable(element));
    }
    public boolean seeText(WebElement element, String textShow ,int timeout){
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(timeout));
       return wait.until(ExpectedConditions.textToBePresentInElement(element,textShow));
    }
    public void waitElementInvisibility(WebElement element, int timeout){
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(timeout));
        wait.until(ExpectedConditions.invisibilityOf(element));
    }
    public void forceClick(WebElement element){
        waitElementVisibility(element,10);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].click()",element);
    }

    public void openInventoryPage(){
        INVENTORY_LABEL.click();
        waitElementVisibility(INVENTORY_PAGE,20);
    }
    public void openManufacturePage(){
       clickElement(MANUFACTURE_LABEL);
        waitElementVisibility(MANUFACTURE_PAGE,20);
    }
    public void clickElement(WebElement objPath){
        waitElementClickable(objPath, 15);
        objPath.click();
    }
    public void openApplication(){
        clickElement(APPLICATION_ICON);
    }

    public void fillFieldElement(WebElement objPath,String keyToSend){
        waitElementVisibility(objPath,15);
        objPath.clear();
        objPath.sendKeys(keyToSend);
    }

    public void openProductPage(){
        waitElementVisibility(PRODUCT_DROPDOWN,10);
        PRODUCT_DROPDOWN.click();
        waitElementClickable(PRODUCT_MENU,20);
        PRODUCT_MENU.click();
        waitElementVisibility(PAGE_TITLE_NAVIGTION,20);
        String title = PAGE_TITLE_NAVIGTION.getText();
        Assert.assertEquals(title, "Products");
    }

    public void clickButtonCreate(){
        clickElement(CREATE_BTN);
    }

    public void clickSaveButton(){
        clickElement(SAVE_BTN);
    }
    public String getCurrentDate(int datePlus){
        DateFormat dtf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        Date currentDate = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(currentDate);
        c.add(Calendar.DATE,datePlus);
        Date currentDatePlusOne = c.getTime();
        return dtf.format(currentDatePlusOne);
    }
    public static String getBarcode(){
        long number = System.currentTimeMillis();
        String convertnumber = Long.toString(number);
        String barcode = convertnumber.substring(4,12);
        return barcode;
    }
    public void wait(int seconds) {
        try {
            Thread.sleep((long)(seconds * 1000));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

}