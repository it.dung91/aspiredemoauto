package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage extends GeneralPage {
    WebDriver driver;
    @FindBy(xpath = "//input[@placeholder='Email']")
    WebElement EMAIL_TXT;
    @FindBy(xpath ="//input[@placeholder='Password']")
    WebElement PASSEWORD_TXT;
    @FindBy(xpath = "//button[normalize-space()='Log in']")
    WebElement LOGIN_BTN;
    @FindBy(xpath = "//div[normalize-space()='Inventory']")
    WebElement INVENTORY_LABEL;

    public LoginPage(WebDriver driver){
        super(driver);
    }
    public void sendKeyEmail(String userName){
        fillFieldElement(EMAIL_TXT,userName);
    }
    public void sendKeyPassword(String passWord){
        fillFieldElement(PASSEWORD_TXT,passWord);
    }
    public void clickLogin(){
        clickElement(LOGIN_BTN);
    }

    public void loginMethod(String userName, String passWord){
        sendKeyEmail(userName);
        sendKeyPassword(passWord);
        clickLogin();
        waitElementVisibility(INVENTORY_LABEL,10);
    }


}
