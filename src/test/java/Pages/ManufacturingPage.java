package Pages;

import io.qameta.allure.Step;
import org.checkerframework.checker.units.qual.C;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import javax.xml.xpath.XPath;

public class ManufacturingPage extends GeneralPage{
    WebDriver driver;

    @FindBy(xpath = "//span[@placeholder='Manufacturing Reference']")
    WebElement MANUFACTURING_TITLE;
    @FindBy(xpath = "//div[@name='product_id']//input")
    WebElement PRODUCT_TXT;
    @FindBy(xpath = "(//li[@class='ui-menu-item'])[1]")
    WebElement PRODUCT_ITEM;
    @FindBy(xpath = "(//input[@name='product_qty'])[1]")
    WebElement QUANTITY_TXT;
    @FindBy(xpath = "//div[@name='product_uom_id']//input")
    WebElement QTY_MEASURE_TXT;
    @FindBy(xpath = "//div[@name='bom_id']//input")
    WebElement BILL_OF_METERIAL_TXT;
    @FindBy(xpath = "//h4[@class='modal-title'][normalize-space()='Create: Bill of Material']")
    WebElement POPUP_CREATE_TITLE;
    @FindBy(xpath = "(//a[contains(normalize-space(),'Create')])[1]")
    WebElement CREATE_MARETIAL_DRP_BTN;
    @FindBy(xpath = "(//button[normalize-space()='Save'])[2]")
    WebElement SAVE_POPUP_BTN;
    @FindBy(xpath = "//div[@name='date_planned_start']//input")
    WebElement SCHEDULE_DATE_TXT;
    @FindBy(xpath = "(//a[normalize-space()='Add a line'])[1]")
    WebElement COMPONENT_ADD_PRODUCT_BTN;
    @FindBy(xpath = "(//div[@name='product_id']//input)[2]")
    WebElement PRODUCT_LINE_TXT;
    @FindBy(xpath = "(//a[contains(normalize-space(),'Search More')]/..)[2]")
    WebElement PRODUCT_SEARCH_MORE_BTN;
    @FindBy(xpath = "//input[@name='product_uom_qty']")
    WebElement PRODUCT_CONSUME_TXT;
    @FindBy(xpath = "//li[@class='nav-item']/a[normalize-space()='Miscellaneous']")
    WebElement MISCELLANEOUS_BTN;
    @FindBy(xpath = "//li[@class='nav-item']/a[normalize-space()='Components']")
    WebElement COMPONENT_BTN;
    @FindBy(xpath = "//input[@name='origin']")
    WebElement SOURCE_TXT;
    @FindBy(xpath = "//span[normalize-space()='Confirm']/..")
    WebElement CONFIRM_BTN;
    @FindBy(xpath = "//span[normalize-space()='Mark as Done']/..")
    WebElement DONE_BTN;
    @FindBy(xpath = "//div[@class='o_Message_prettyBody'][normalize-space()='Production Order created']")
    WebElement CREATED_LOG;
    @FindBy(xpath = "//span[normalize-space()='Apply']/..")
    WebElement APPLY_BTN;
    @FindBy(xpath = "//button[@title='Current state']")
    WebElement CURRENT_STATE;
    //Detail page
    @FindBy(xpath = "//a[@name='product_id']/span")
    WebElement DT_PRODUCT_NAME;
    @FindBy(xpath = "//div/span[@name='product_qty']")
    WebElement DT_PRODUCT_QTY;
    @FindBy(xpath = "//div/span[@name='qty_producing']")
    WebElement PRODUCING_QTY;
    @FindBy(xpath = "//span[@name='product_uom_id']")
    WebElement DT_PRODUCT_MEASURE;
    @FindBy(xpath = "//a[@name='bom_id']/span")
    WebElement DT_BILL_O_MATERIAL;
    @FindBy(xpath = "//span[@name='date_planned_start']")
    WebElement DT_PLAN_DATE;
    @FindBy(xpath = "//a[@name='user_id']/span")
    WebElement DT_RESPONSIBLE;
    @FindBy(xpath = "//span[@name='product_uom_qty']/../preceding-sibling::td")
    WebElement DT_CP_PRODUCT_NAME;
    @FindBy(xpath = "//span[@name='product_uom_qty']")
    WebElement DT_CP_PRODUCT_QTY;
    @FindBy(xpath = "//span[@name='product_uom_qty']/../following-sibling::td[1]")
    WebElement DT_CP_PRODUCT_MEASURE;
    @FindBy(xpath = "//a[@name='location_src_id']")
    WebElement DT_COMPONENT_LOCATION;
    @FindBy(xpath = "//a[@name='location_dest_id']")
    WebElement DT_FINISHED_LOCATION;
    @FindBy(xpath = "//span[@name='origin']")
    WebElement DT_SOURCE;



    public ManufacturingPage(WebDriver driver) {
        super(driver);
    }

    @Step("Create Manufacturing")
    public void createManufacturing(String productName, String quantity,String consume){
        clickButtonCreate();
        seeText(MANUFACTURING_TITLE, "New",10);
        fillFieldElement(PRODUCT_TXT,productName);
        wait(2);
        PRODUCT_TXT.sendKeys(Keys.ENTER);
        //add date
//        fillFieldElement(SCHEDULE_DATE_TXT,date);
        //Update Bill meterial
        fillFieldElement(BILL_OF_METERIAL_TXT,productName);
        wait(2);
        BILL_OF_METERIAL_TXT.sendKeys(Keys.ENTER);
        clickElement(SAVE_POPUP_BTN);
        //Update Qty
        waitElementInvisibility(POPUP_CREATE_TITLE,30);
        fillFieldElement(QUANTITY_TXT,quantity);
        //Add new Line product
        clickElement(COMPONENT_ADD_PRODUCT_BTN);
        fillFieldElement(PRODUCT_LINE_TXT,productName);
        wait(2);
        PRODUCT_LINE_TXT.sendKeys(Keys.ENTER);
        fillFieldElement(PRODUCT_CONSUME_TXT,consume);
        PRODUCT_CONSUME_TXT.sendKeys(Keys.TAB);
        clickElement(MISCELLANEOUS_BTN);
        fillFieldElement(SOURCE_TXT,productName);
        clickSaveButton();
        waitElementVisibility(CREATED_LOG,10);
        seeText(MANUFACTURING_TITLE,"WH/MO/",5);

    }
    @Step("Change Manufacturing to Done")
    public void markDoneManufacturing(){
        clickElement(CONFIRM_BTN);
        waitElementInvisibility(CONFIRM_BTN,10);
        clickElement(DONE_BTN);
        clickElement(APPLY_BTN);
        wait(3);
        seeText(CURRENT_STATE,"DONE",10);
    }
    @Step("Check Manufacturing detail")
    public void checkManufacturingDetail(String productName, String quantity,String consume,String measure,String location, String source){
        Assert.assertEquals(DT_COMPONENT_LOCATION.getText(),location);
        Assert.assertEquals(DT_FINISHED_LOCATION.getText(),location);
        Assert.assertEquals(DT_SOURCE.getText(), source);

        Assert.assertEquals(DT_PRODUCT_NAME.getText(),productName);
//        waitElementVisibility(DT_PRODUCT_QTY,5);
        Assert.assertEquals(DT_PRODUCT_QTY.getText(),quantity);
//        waitElementVisibility(DT_PRODUCT_QTY,5);
        Assert.assertEquals(DT_PRODUCT_QTY.getText(),quantity);
//        waitElementVisibility(DT_PRODUCT_QTY,5);
        Assert.assertEquals(DT_PRODUCT_QTY.getText(),quantity);
        //Check component tab
        clickElement(COMPONENT_BTN);
        Assert.assertEquals(DT_PRODUCT_MEASURE.getText(),measure);
        Assert.assertEquals(DT_CP_PRODUCT_NAME.getText(),productName);
        Assert.assertEquals(DT_CP_PRODUCT_QTY.getText(),consume);

    }

}
