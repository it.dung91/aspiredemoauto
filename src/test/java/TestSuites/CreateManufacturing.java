package TestSuites;

import Pages.ManufacturingPage;
import Pages.CreateProductPage;
import Pages.GeneralPage;
import Pages.LoginPage;
import Variable.Variables;
import io.github.bonigarcia.wdm.WebDriverManager;
import io.qameta.allure.Description;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.*;

public class CreateManufacturing {

    WebDriver driver;
    LoginPage loginPage;
    GeneralPage generalPage;
    CreateProductPage createProductPage;
    ManufacturingPage manufacturingPage;
    Variables var = new Variables();

    @BeforeTest
    public void openApplication(){
        WebDriverManager.chromedriver().setup();
        ChromeOptions options = new ChromeOptions();
        options.setHeadless(var.HEADLESS);
        options.addArguments("window-size=1400,800");
        options.addArguments("disable-gpu");
         driver = new ChromeDriver(options);
        driver.get(var.URL);
    }
    @Test(priority = 0)
    @Description("Login to App")
    public void logintoWebSuccessfully(){
        loginPage = new LoginPage(driver);
        loginPage.loginMethod(var.userName, var.passWord);
    }

    @Test(priority =1)
    @Description("Create Product and update Quantity Successfully")
    public void Test_CreateProductAndUpdateQuantity(){
        generalPage = new GeneralPage(driver);
        createProductPage = new CreateProductPage(driver);
        manufacturingPage = new ManufacturingPage(driver);
        generalPage.openInventoryPage();
        generalPage.openProductPage();
        generalPage.clickButtonCreate();
        createProductPage.fillProductInformation(var.productName, var.type_Storable, var.inReference,var.inNote,generalPage.getBarcode(),var.cost);
        createProductPage.fillInventoryData(var.weight,var.volumne,var.manufacture_time, var.customer_time);
        createProductPage.addPackaging(var.packagingName,var.qty, generalPage.getBarcode());
        createProductPage.clickSaveButton();
        createProductPage.verifyCreatedProduct();
    }

    @Test(priority = 2)
    @Description("Update Quantity after created product")
    public void Test_updateQuantity(){
        createProductPage.updateQuantity(var.qtyLOCATION, "5",var.qty);
    }

    @Test(priority = 3)
    @Description("Run test Create Product and Manufacturing Order")
    public void Test_CreateManufacturing(){
        generalPage.openApplication();
        generalPage.openManufacturePage();
        manufacturingPage.createManufacturing(var.productName,var.qty,var.consume);
        manufacturingPage.markDoneManufacturing();
    }

    @Test(priority = 4)
    @Description("Verify the Create Manufacturing order")
    public void Test_verifyCreatedManufacturing(){
        manufacturingPage.checkManufacturingDetail(var.getProductName(), var.qty,var.consume,var.units,var.qtyLOCATION, var.productName );
    }

    @AfterTest
    public void close(){
        driver.quit();
    }
}
